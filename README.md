```
npm install
bower install
jsx --harmony --watch src/ build
```

## TODO

Using 2-3 hours, please tackle as many of these as you wish:

- Render list of cakes
- Update to pass eslint (http://eslint.org/) to run: eslint src
- Implement a loading state
- Investigate using the Flux (Flux or Redux implentation) pattern with a CakeStore and dispatcher
- Make UI render nicely on iPad and Nexus 5
- Add ability to search cakes
- Add basic ability to edit/add cakes
- Add tests

## Completed

- render list of cakes
- elint passes
- basic loading screen
- uses Redux
- renders "nicely" on both iPad and Nexus 5 emulator
- added client search filter on initial cakes load instead of a search that goes to backend
- added edit/add feature
- added jest/jasmine
- added grunt tooling
- added heroku deployment

```
npm install && bower install
grunt dev
```

executes localhost:3333