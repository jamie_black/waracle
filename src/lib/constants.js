'use strict';
var keyMirror = require('key-mirror');

var constants = keyMirror({
  FETCH_CAKES_REQUEST: null,
  FETCH_CAKES_SUCCESS: null,
  FETCH_CAKES_FAILURE: null,

	FILTER_CAKES: null,
	EDIT_CAKE: null,
	EDIT_CANCEL_CAKE: null,
	EDIT_SAVE_CAKE: null,

  ADD_CAKE: null,
});

module.exports = constants;
