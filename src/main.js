'use strict';

var React = require('react');
var ReactDOM = require('react-dom');
var Provider = require('react-redux').Provider;
var createStore = require('redux').createStore;
var applyMiddleware = require('redux').applyMiddleware;
var thunk = require('redux-thunk').default;
var reducer = require('./reducers/cakes/cakeReducer');
var initialState = require('./reducers/cakes/cakesInitialState');
var store = createStore(reducer, initialState, applyMiddleware(thunk));
var CakeListContainer = require('./containers/cakeListContainer-connected');

var Main = React.createClass({
	render: function() {
		return (
			<Provider store={store}>
				<CakeListContainer />
			</Provider>
		);
	}
});

ReactDOM.render(<Main />, document.getElementById('cakeList'));
