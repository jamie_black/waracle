'use strict';
jest.autoMockOff();

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var CakeListContainer = require('../cakeListContainer');

describe('given a cake container', function() {
	var initialState;

	beforeEach(function() {
		initialState = {
			cakes: []
		};
	});

	it('should fire action to get cakes on mount', function() {
		var mockActions = jasmine.createSpyObj('actions', ['fetchCakes']);
		mockActions.fetchCakes.and.callFake(function(){});
		TestUtils.renderIntoDocument(<CakeListContainer cakes={initialState.cakes} actions={mockActions}/>);
		expect(mockActions.fetchCakes).toHaveBeenCalled();
	});

	it('should render warning if fetch fails', function() {
		var mockActions = jasmine.createSpyObj('actions', ['fetchCakes']);
		mockActions.fetchCakes.and.callFake(function(){

		});
		var container = TestUtils.renderIntoDocument(<CakeListContainer cakes={initialState.cakes} actions={mockActions} error={true} fetching={false}/>);
		var containerDOM = ReactDOM.findDOMNode(container);
		expect(containerDOM.tagName).toBe('H1');
	});

	it('should fire action to get cakes on mount', function() {
		var mockActions = jasmine.createSpyObj('actions', ['fetchCakes']);
		mockActions.fetchCakes.and.callFake(function(){});
		TestUtils.renderIntoDocument(<CakeListContainer cakes={initialState.cakes} actions={mockActions}/>);
		expect(mockActions.fetchCakes).toHaveBeenCalled();
	});

	it('should render warning if fetch fails', function() {
		var mockActions = jasmine.createSpyObj('actions', ['fetchCakes']);
		mockActions.fetchCakes.and.callFake(function(){

		});
		var container = TestUtils.renderIntoDocument(<CakeListContainer cakes={initialState.cakes} actions={mockActions} error={false} fetching={true}/>);
		var containerDOM = ReactDOM.findDOMNode(container);
		expect(containerDOM.tagName).toBe('H1');
	});
});
