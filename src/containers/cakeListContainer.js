'use strict';

var React = require('react');
var CakeList = require('../components/cake-list/cakeList');
var Filter = require('../components/filter/filter');

var CakeListContainer = React.createClass({

	propTypes: {
		cakes: React.PropTypes.array,
		actions: React.PropTypes.object,
		filter: React.PropTypes.string,
		editCake: React.PropTypes.object,
		fetching: React.PropTypes.bool,
		error: React.PropTypes.bool
	},

	render: function () {
		var entryPoint;
		if(this.props.error) {
			entryPoint = <h1>error</h1>;
		} else if(this.props.fetching){
			entryPoint = <h1>fetching</h1>;
		} else {
			entryPoint = (
				<div>
					<Filter actions={this.props.actions} filter={this.props.filter} />
					<CakeList cakes={this.props.cakes} actions={this.props.actions} editCake={this.props.editCake}/>
				</div>
			);
		}
		return (
			entryPoint
		)
	},
	componentDidMount: function () {
    this.props.actions.fetchCakes();
	}
});

module.exports = CakeListContainer;
