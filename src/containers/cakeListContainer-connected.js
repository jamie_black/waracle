var React = require('react');
var urls = require('../lib/urls');
var bindActionCreators = require('redux').bindActionCreators;
var connect = require('react-redux').connect;
var actions = require('../reducers/cakes/cakeActions');

var CakeListContainer = require('./cakeListContainer');

function mapStateToProps(state) {
	return {
		fetching: state.fetching,
		cakes: state.cakes,
		error: state.error,
		filter: state.filter,
		editCake: state.editCake
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(actions, dispatch)
	};
}


module.exports = connect(mapStateToProps, mapDispatchToProps)(CakeListContainer);
