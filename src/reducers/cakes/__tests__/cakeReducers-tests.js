'use strict';
jest.autoMockOff();

var actions = require('../cakeActions');
var constants = require('../../../lib/constants');
var reducerUnderTest = require('../cakeReducer');

describe('given cake reducers', function() {

	it('should update fetching state for cakes', function() {
		var action = actions.fetchCakesRequest();
		var state = reducerUnderTest({}, action);
		expect(state.fetching).toBe(true);
	});

	it('should update fetched cakes', function() {
		var cakes = [
			{}
		];

		var action = actions.fetchCakesSuccess(cakes);
		var state = reducerUnderTest({}, action);
		expect(state.cakes).toEqual(cakes);
	});

	it('should update failure state', function() {
		var error = {
			message: 'No more cake for you!'
		};

		var action = actions.fetchCakesFailure(error);
		var state = reducerUnderTest({}, action);
		expect(state.error).toBe(true);
		expect(state.errorMessage).toBe(error.message);
	});

	it('should return orignal state', function() {
		var initialState = {
			cakes: []
		};

		var action = {
			type: 'NO_SUCH_ACTION'
		};

		var state = reducerUnderTest(initialState, action);
		expect(initialState).toBe(state);
	});

	it('should return filtered cakes', function() {
		var initialState = {
			cakes: [{
				title: 'sponge cake'
			},{
				title: 'carrot'
			},{
				title: 'sponge cake'
			},{
				title: 'carrot'
			}]
		};

		var action = {
			type: constants.FILTER_CAKES,
			payload: 'sponge'
		};

		var filtered = initialState.cakes.filter(function(cake){
			return cake.title.includes('sponge');
		});
		var state = reducerUnderTest(initialState, action);
		expect(filtered).toEqual(state.cakes);
		expect(state.originalCakes).toEqual(initialState.cakes);
	});

	it('should be able to add cake', function() {
		var initialState = {
			cakes: [ ]
		};

		var action = {
			type: constants.ADD_CAKE
		};

		var state = reducerUnderTest(initialState, action);

		expect(state.editCake).toEqual({
			title: '',
			image: '',
			desc: ''
		});

		expect(state.cakes.length).toBe(1);
	});

	it('should be able to edit cake', function() {
		var initialState = {
			cakes: [ {
				title: 'dodgy cake'
			}]
		};

		var action = {
			type: constants.EDIT_CAKE,
			payload: initialState.cakes[0]
		};

		var state = reducerUnderTest(initialState, action);

		expect(state.editCake).toEqual(action.payload);
	});

	it('should be able cancel edit cake', function() {
		var initialState = {
			cakes: [ {
				title: 'dodgy cake'
			}]
		};

		var action = {
			type: constants.EDIT_CANCEL_CAKE,
			payload: initialState.cakes[0]
		};

		var state = reducerUnderTest(initialState, action);

		expect(state.editCake).toBe(null);
	});

	it('should be able save edit cake', function() {
		var cakeToEdit = {
			title: 'dodgy cake'
		};
		var initialState = {
			cakes: [ cakeToEdit ],
			editCake: cakeToEdit
		};

		var action = {
			type: constants.EDIT_SAVE_CAKE,
			payload: {
				title: 'awesome cake',
				image: 'http://placecake.com',
				desc: 'red velvet'
			}
		};

		var state = reducerUnderTest(initialState, action);

		expect(state.editCake).toBe(null);
		expect(state.cakes[0]).toEqual(action.payload);
	});

	it('should be able save edit cake with filter on', function() {
		var cakeToEdit = {
			title: 'dodgy cake'
		};
		var cakes = [
			cakeToEdit
		];
		var initialState = {
			cakes: cakes,
			originalCakes: cakes,
			editCake: cakeToEdit
		};

		var action = {
			type: constants.EDIT_SAVE_CAKE,
			payload: {
				title: 'awesome cake',
				image: 'http://placecake.com',
				desc: 'red velvet'
			}
		};

		var state = reducerUnderTest(initialState, action);

		expect(state.editCake).toBe(null);
		expect(state.cakes[0]).toEqual(action.payload);
	});
});
