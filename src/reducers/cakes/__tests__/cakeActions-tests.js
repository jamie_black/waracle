'use strict';
jest.autoMockOff();

var $ = require('jquery');
var constants = require('../../../lib/constants');
var actions = require('../cakeActions');
var configureStore = require('redux-mock-store');
var thunk = require('redux-thunk').default;
var middlewares = [thunk];
var mockStore = configureStore(middlewares);

describe('given a set of cake actions', function() {
	it('should have fetching action', function(){
		var action = actions.fetchCakesRequest();
		expect(action.type).toBe(constants.FETCH_CAKES_REQUEST);
	});

	it('should have fetch action', function(){
		var action = actions.fetchCakesSuccess();
		expect(action.type).toBe(constants.FETCH_CAKES_SUCCESS);
	});

	it('should have fetch failure action', function(){
		var error = {
			message: 'No more cake for you'
		};

		var action = actions.fetchCakesFailure(error);
		expect(action.type).toBe(constants.FETCH_CAKES_FAILURE);
		expect(action.payload).toBe(error.message);
	});

	it('should have add cake action', function(){
		var action = actions.addCake();
		expect(action.type).toBe(constants.ADD_CAKE);
	});

	it('should be able to fetch cakes', function(done) {
		var store = mockStore({});
		var cakesToFetch = JSON.stringify([{}]);
		spyOn($, 'ajax').and.callFake(function () {
			var d = $.Deferred();
			d.resolve(cakesToFetch);
			return d.promise();
		});

		var action = actions.fetchCakes();
		store.dispatch(action).then(function(){
			var actionsRecorded = store.getActions();

			expect(actionsRecorded[0]).toEqual(actions.fetchCakesRequest());
			expect(actionsRecorded[1]).toEqual(actions.fetchCakesSuccess(JSON.parse(cakesToFetch)));
			done();
		});
	});

	it('should handle failure of cake fetch', function(done) {
		var store = mockStore();
		var errorMsg = {
			message: 'No more cake for you!'
		};

		spyOn($, 'ajax').and.callFake(function () {
			var d = $.Deferred();
			d.reject(errorMsg);
			return d.promise();
		});

		var action = actions.fetchCakes();
		store.dispatch(action).then(function(){
		})
		.fail(function() {
			var actionsRecorded = store.getActions();
			expect(actionsRecorded[0]).toEqual(actions.fetchCakesRequest());
			expect(actionsRecorded[1]).toEqual(actions.fetchCakesFailure(errorMsg));
			done();
		});
	});

	it('should have filter action for cakes', function() {
		var filter = 'cake';
		var action = actions.filterCakes(filter);
		expect(action.type).toBe(constants.FILTER_CAKES);
		expect(action.payload).toBe(filter);
	});

	it('should have filter action for cakes', function(done) {
		var store = mockStore({});
		var initialState = {
			cakes: [{
				title: 'sponge cake'
			},{
				title: 'carrot'
			},{
				title: 'sponge cake'
			},{
				title: 'carrot'
			}]
		};
		var str = JSON.stringify(initialState.cakes);
		spyOn($, 'ajax').and.callFake(function () {
			var d = $.Deferred();
			d.resolve(str);
			return d.promise();
		});

		var action = actions.fetchCakes();
		store.dispatch(action).then(function(){
			var actionsRecorded = store.getActions();

			expect(actionsRecorded[0]).toEqual(actions.fetchCakesRequest());
			expect(actionsRecorded[1]).toEqual(actions.fetchCakesSuccess(initialState.cakes));
			done();
		});
	});

	it('should have edit cake action', function() {
		var cake = {
			title: 'dodgy cake'
		};
		var action = actions.editCake(cake);
		expect(action.type).toBe(constants.EDIT_CAKE);
		expect(action.payload).toBe(cake);
	});

	it('should have cancel edit cake action', function() {
		var cake = {
			title: 'dodgy cake'
		};
		var action = actions.editCancelCake(cake);
		expect(action.type).toBe(constants.EDIT_CANCEL_CAKE);
		expect(action.payload).toBe(cake);
	});

	it('should have save edit cake action', function() {
		var cake = {
			title: 'dodgy cake'
		};
		var action = actions.editSaveCake(cake);
		expect(action.type).toBe(constants.EDIT_SAVE_CAKE);
		expect(action.payload).toBe(cake);
	});
});
