'use strict';
var $ = require('jquery');
var constants = require('../../lib/constants');
var urls = require('../../lib/urls');

var fetchCakesRequest = function () {
  return {
    type: constants.FETCH_CAKES_REQUEST
  }
}

var fetchCakesSuccess = function (body) {
  return {
    type: constants.FETCH_CAKES_SUCCESS,
    payload: body
  }
}

var fetchCakesFailure = function (error) {
  return {
    type: constants.FETCH_CAKES_FAILURE,
    payload: error.message
  }
}

var filterCakes = function (filter) {
	return {
		type: constants.FILTER_CAKES,
		payload: filter
	};
}

var addCake = function() {
  return {
    type: constants.ADD_CAKE
  }
}

var fetchCakes = function() {
  return function(dispatch) {

    dispatch(fetchCakesRequest());

    return $.ajax(urls.CAKE_URL)
    .then(function(data) {
      dispatch(fetchCakesSuccess(JSON.parse(data)));
    })
    .fail(function(err) {
      dispatch(fetchCakesFailure(err))
    });
  };
}

var editCake = function(cake) {
	return {
		type: constants.EDIT_CAKE,
		payload: cake
	};
}

var editCancelCake = function(cake) {
	return {
		type: constants.EDIT_CANCEL_CAKE,
		payload: cake
	};
}

var editSaveCake = function(cake) {
	return {
		type: constants.EDIT_SAVE_CAKE,
		payload: cake
	};
}

module.exports = {
  fetchCakesRequest: fetchCakesRequest,
  fetchCakesSuccess: fetchCakesSuccess,
  fetchCakesFailure: fetchCakesFailure,
  addCake: addCake,
  fetchCakes: fetchCakes,
	filterCakes: filterCakes,
	editCake: editCake,
	editCancelCake: editCancelCake,
	editSaveCake: editSaveCake
};
