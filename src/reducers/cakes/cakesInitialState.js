'use strict';

var initialState = {
  fetching: false,
  cakes: [],
	error: false,
	filter: '',
	editCake: null
}

module.exports = initialState;
