'use strict';

var constants = require('../../lib/constants');

var cakeReducer = function(state, action) {
	var nextState;
	switch (action.type) {
		case constants.FETCH_CAKES_REQUEST:
			nextState = Object.assign({}, state, {
				fetching: true
			});
			return nextState;
		case constants.FETCH_CAKES_SUCCESS:
			nextState = Object.assign({}, state, {
				fetching: false,
				cakes: action.payload
			});
			return nextState;
		case constants.FETCH_CAKES_FAILURE:
			nextState = Object.assign({}, state, {
				error: true,
				fetching: false,
  			errorMessage: action.payload
			});
			return nextState;
		case constants.FILTER_CAKES:
			var originalCakes = state.originalCakes ? state.originalCakes : state.cakes;

			var filtered = originalCakes.filter(function (cake) {
				return cake.title.toUpperCase().includes(action.payload.toUpperCase());
			});

			nextState = Object.assign({}, state, {
				cakes: filtered,
				filter: action.payload,
				originalCakes : originalCakes
			});
			return nextState;
		case constants.ADD_CAKE:
			var cakeToAdd = {
				title: '',
				image: '',
				desc: ''
			};

			var cakesCollection = getCakesCollectionWithFlag(state);

			cakesCollection.cakes.unshift(cakeToAdd);

			nextState = addCakesToNewState(cakesCollection.isFiltered, cakesCollection.cakes, state);
			nextState.editCake = cakeToAdd;
			return nextState;
		case constants.EDIT_CAKE:
			nextState = Object.assign({}, state, {
				editCake: action.payload
			});
			return nextState;
		case constants.EDIT_CANCEL_CAKE:
			nextState = Object.assign({}, state, {
				editCake: null
			});
			return nextState;
		case constants.EDIT_SAVE_CAKE:
			var cakesCollection = getCakesCollectionWithFlag(state);
			var indexOfEditCake = cakesCollection.cakes.indexOf(state.editCake);
			cakesCollection.cakes[indexOfEditCake] = action.payload;

			return addCakesToNewState(cakesCollection.isFiltered, cakesCollection.cakes, state);
    default:
      return state;
  }

	function getCakesCollectionWithFlag(state) {
		var cakes = state.originalCakes ? state.originalCakes : state.cakes;
		var isFiltered = state.hasOwnProperty('originalCakes');
		return {
			isFiltered: isFiltered,
			cakes: cakes
		};
	}

	function addCakesToNewState(isFiltered, cakes, state) {
		var nextState;
		if(isFiltered) {
			nextState = Object.assign({}, state, {
				editCake: null,
				originalCakes: cakes
			});
		} else {
			nextState = Object.assign({}, state, {
				editCake: null,
				cakes: cakes
			});
		}

		return nextState;
	}
};

module.exports = cakeReducer;
