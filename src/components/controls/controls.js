'use strict';
var React = require('react');

var Controls = React.createClass({

	propTypes: {
		actions: React.PropTypes.object
	},

	onClick: function() {
		this.props.actions.addCake();
	},

	render: function() {
		return <button className="btn btn-default add" onClick={this.onClick}>add</button>
	}
});

module.exports = Controls;
