'use strict';
jest.autoMockOff();

var React = require('react');
var TestUtils = require('react-addons-test-utils');
var Controls = require('../controls');

describe('given controls', function () {
	var component,
		mockActions;

	beforeEach(function() {
		mockActions = jasmine.createSpyObj('actions', ['addCake']);
		var renderer = TestUtils.createRenderer();
		renderer.render(<Controls actions={mockActions} />);
		component = renderer.getRenderOutput();
		component.props.onClick();
	});

	it('should have add btn', function() {
		expect(component).toBeDefined();
	});

	it('should fire add cake actions', function(){
		expect(mockActions.addCake).toHaveBeenCalled();
	});
});
