'use strict';
jest.autoMockOff();

var React = require('react');
var TestUtils = require('react-addons-test-utils');
var Filter = require('../filter');

describe('given a filter component', function() {
	var filterComponent,
		mockActions;

	beforeEach(function () {
		mockActions = jasmine.createSpyObj('actions', ['filterCakes']);
		filterComponent = TestUtils.renderIntoDocument(<Filter filter={''} actions={mockActions}/>);
	});

	it('should call filter action with input', function() {
		var node = filterComponent.refs.filterInputField;
		node.value = 'cake';
		TestUtils.Simulate.change(node);
		expect(mockActions.filterCakes).toHaveBeenCalled();
	});
});
