'use strict';
var React = require('react');
var findDOMNode = require('react-dom').findDOMNode;
var Controls = require('../controls/controls')

var Filter = React.createClass({

	propTypes: {
		filter: React.PropTypes.string,
		actions: React.PropTypes.object
	},

	componentDidMount: function() {
		findDOMNode(this.refs.filterInputField).focus();
	},

	handleChange: function(ev) {
		this.props.actions.filterCakes(ev.target.value);
	},

	render: function () {
		return (
				<div className="filter">
					<input className="filter__input"
						type="text"
						onChange={this.handleChange}
						ref="filterInputField"
						value={this.props.filter}
						placeholder="Filter cakes..." />
					<Controls actions={this.props.actions} />
				</div>
    );
	},
});

module.exports = Filter;
