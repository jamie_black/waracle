'use strict';
jest.autoMockOff();

var React = require('react');
var TestUtils = require('react-addons-test-utils');
var CakeListItem = require('../cakeListItem');

describe('given a cake', function() {
	var listItem,
		cake;

	beforeEach(function() {
		cake = {
			image: 'http://placecake.com/',
			title: 'what a lovely cake!'
		};
		var renderer = TestUtils.createRenderer();
		renderer.render(<CakeListItem cake={cake} />);
		listItem = renderer.getRenderOutput();
	});

	it('should render a list item', function() {
		expect(listItem.type).toBe('li');
	});

	it('should have an child img tag', function() {
		var img = listItem.props.children.props.children.filter(function(el) {
			return el.type === 'img';
		});
		expect(img.length).toBe(1);
	});

	it('should render a list item with cake values set', function() {
		var img = listItem.props.children.props.children.filter(function(el) {
			return el.type === 'img';
		});

		var span = listItem.props.children.props.children.filter(function(el) {
			return el.type === 'span';
		});

		expect(img[0].props.src).toBe(cake.image);
		expect(span[0].props.children).toBe(cake.title);
	});

	it('should render in edit mode', function() {
		var renderer = TestUtils.createRenderer();
		renderer.render(<CakeListItem cake={cake} edit={true} />);
		listItem = renderer.getRenderOutput();
		var h1 = listItem.props.children.props.children.filter(function(el) {
			return el.type === 'h1';
		});
		expect(h1).toBeDefined();
	});

	it('should handle request to edit cake', function() {
		var mockedActions = jasmine.createSpyObj('actions', ['editCake']);
		var renderer = TestUtils.createRenderer();
		renderer.render(<CakeListItem cake={cake} edit={false} actions={mockedActions}/>);
		listItem = renderer.getRenderOutput();
		var anchor = listItem.props.children;
		anchor.props.onClick();
		expect(mockedActions.editCake).toHaveBeenCalled();
	});

	it('should handle request to cancel edit cake', function() {
		var mockedActions = jasmine.createSpyObj('actions', ['editCancelCake']);
		var renderer = TestUtils.createRenderer();
		renderer.render(<CakeListItem cake={cake} edit={true} actions={mockedActions}/>);
		listItem = renderer.getRenderOutput();
		var cancel = listItem.props.children.props.children.filter(function(child) {
			return child.props.className.includes('cancel');
		});
		cancel[0].props.onClick({preventDefault: function () {
		}});
		expect(mockedActions.editCancelCake).toHaveBeenCalled();
	});

	it('should handle request to save edit cake', function() {
		var mockedActions = jasmine.createSpyObj('actions', ['editSaveCake']);
		var listItem = TestUtils.renderIntoDocument(<CakeListItem cake={cake} edit={true} actions={mockedActions}/>);
		
		listItem.refs.imageInput = 'image';
		listItem.refs.titleInput = 'title';
		listItem.refs.descInput = 'des';

		listItem.onSave({preventDefault:function() {
		}});
		expect(mockedActions.editSaveCake).toHaveBeenCalled();
	});
});
