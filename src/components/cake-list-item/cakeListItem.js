'use strict';
var React = require('react');

var CakeList = React.createClass({

	propTypes: {
		cake: React.PropTypes.object,
		edit: React.PropTypes.bool,
		actions: React.PropTypes.object,
		key: React.PropTypes.string
	},

	onClick: function() {
		this.props.actions.editCake(this.props.cake);
	},

	onCancel: function(e) {
		e.preventDefault();
		this.props.actions.editCancelCake(this.props.cake);
	},

	onSave: function(e) {
		e.preventDefault();
		this.props.actions.editSaveCake({
			image: this.refs.imageInput.value,
			title: this.refs.titleInput.value,
			desc: this.refs.descInput.value
		});
	},

	render: function () {
		var component;

		if(this.props.edit) {
			component = (
				<form className="form-horizontal cake-item">
					<div className="form-group">
						<label htmlFor="image" className="col-md-4 control-label">Image</label>
						<div className="col-md-8">
							<input type="text" className="form-control" id="image" placeholder="Image URL" defaultValue={this.props.cake.image} ref="imageInput" />
						</div>
					</div>
				  <div className="form-group">
				    <label htmlFor="title" className="col-md-4 control-label">Title</label>
				    <div className="col-md-8">
				      <input type="text" className="form-control" id="title" placeholder="Title" defaultValue={this.props.cake.title} ref="titleInput" />
				    </div>
				  </div>
					<div className="form-group">
				    <label htmlFor="description" className="col-sm-4 control-label">Description</label>
				    <div className="col-md-8">
				      <input type="text" className="form-control" id="description" placeholder="Description" defaultValue={this.props.cake.desc} ref="descInput"/>
				    </div>
				  </div>
					<button className="btn btn-default cancel pull-left" onClick={this.onCancel}>cancel</button>
					<button className="btn btn-primary save pull-right" onClick={this.onSave}>save</button>
					<div className="clearfix"></div>
				</form>
			);
		} else {
			component = (
				<a className="cake-item preview" onClick={this.onClick} title={this.props.cake.desc}>
					<img src={this.props.cake.image} className="cake-item__img" width="120" height="120"/>
					<span className="cake-item__title">{this.props.cake.title}</span>
				</a>
			);
		}
		return (
				<li>
					{component}
				</li>
    );
	},
});

module.exports = CakeList;
