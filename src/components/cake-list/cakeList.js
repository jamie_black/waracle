'use strict';
var React = require('react');
var CakeListItem = require('../cake-list-item/cakeListItem');
var _ = require('lodash');

var CakeList = React.createClass({

	propTypes: {
		cakes: React.PropTypes.array,
		actions: React.PropTypes.object,
		editCake: React.PropTypes.object
	},

	render: function () {
		return <ul className="list-inline">{this.props.cakes.map((cake, index) => {
			return <CakeListItem key={index} cake={cake} edit={cake === this.props.editCake} actions={this.props.actions}/>
		})}</ul>;
	}
});

module.exports = CakeList;
