'use strict';
jest.autoMockOff();

var React = require('react');
var TestUtils = require('react-addons-test-utils');
var CakeList = require('../cakeList');

describe('given a list of cakes', function () {
	var list,
		cakes;

	beforeEach(function() {
		cakes = [{
			image: 'http://placecake.com/',
			title: 'what a lovely cake!'
		},{
			image: 'http://placecake.com/',
			title: 'what a lovely cake!'
		},{
			image: 'http://placecake.com/',
			title: 'what a lovely cake!'
		}];

		var renderer = TestUtils.createRenderer();
		renderer.render(<CakeList cakes={cakes} />);
		list = renderer.getRenderOutput();
	});

	it('should render a list container', function () {
		expect(list.type).toBe('ul');
	});

	it('should render list items', function () {
		expect(list.props.children.length).toBe(cakes.length);
	});
});
