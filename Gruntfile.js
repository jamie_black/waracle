module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    connect: {
      server: {
        options: {
          port: process.env.PORT || 3333,
          hostname: '*',
          base: ['./build/', './bower_components/'],
          keepalive: true
        }
      }
    },

    browserify: {
      dev: {
        files: {
          './build/bundle.js': ['src/main.js']
        },
        options: {
          browserifyOptions: {
            debug: true
          },
          transform: [['reactify', { 'es6':true}]]
        }
      }
    },

    jest: {
      options: {
        coverage: true,
        testPathPattern: /.*-tests.js/
      }
    },

    eslint: {
      src: ["./src/main.js", "./src/**/__tests__/*.js"]
	  },

    compass: {
      dev: {
        options: {
          sassDir: './src/styles/',
          cssDir: 'build/'
        }
      }
    },

    copy: {
      dev: {
        files:  [
          {expand: true, src: ['index.html'], dest: 'build/', filter: 'isFile', flatten: true}
        ]
      }
    },

    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      dev: ['watch:dev', 'connect']
    },

    watch: {
      dev: {
        files: ['src/**/*.js', 'src/**/*.html', 'src/**/*.scss', 'src/**/*.json'],
        tasks: ['eslint', 'jest', 'browserify', 'compass', 'copy']
      }
    }
  });

	grunt.registerTask('dev', ['eslint', 'jest', 'browserify', 'compass', 'copy', 'concurrent']);
  grunt.registerTask('default', ['browserify', 'compass', 'copy', 'connect']);
}
